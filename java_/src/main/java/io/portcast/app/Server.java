package io.portcast.app;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import com.google.gson.Gson;

public class Server {
    private static final Logger logging = Logger.getLogger(Server.class.getName());
    private static final String HOST_NAME = "0.0.0.0";
    private static final Integer SERVER_PORT = 10101;
    private static PushCache CACHE = new PushCache();
    private static final Gson gson = new Gson();

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT), 0);
        server.createContext("/", new RootHandler());
        server.start();
        System.out.println("Server started http://" + HOST_NAME + ":" + SERVER_PORT);
    }

    static class RootHandler implements HttpHandler {
        private void setResponseHeaders(HttpExchange exchange, Integer responseCode, Integer responseLength)
                throws IOException {
            exchange.sendResponseHeaders(responseCode, responseLength);
            exchange.getResponseHeaders().set("Content-Type", "application/json");
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String response = "";
            OutputStream os = exchange.getResponseBody();

            if ("GET".equals(exchange.getRequestMethod())) {
                response = doGET(exchange);
            } else if ("POST".equals(exchange.getRequestMethod())) {
                response = doPOST(exchange);
            } else {
                response = "Unsupported HTTP method: " + exchange.getRequestMethod();
            }

            os.write(response.getBytes(StandardCharsets.UTF_8));
            os.close();
        }

        private String doGET(HttpExchange exchange) throws IOException {
            String response = "";

            String path = exchange.getRequestURI().getPath();
            Map<String, List<String>> headers = exchange.getRequestHeaders();
            logging.info(String.format("GET request,%nPath: %s%nHeaders:%n%s", path, headers));

            if ("/latest".equals(exchange.getRequestURI().getPath())) {
                response = gson.toJson(CACHE.getLatest());
            } else if ("/all".equals(exchange.getRequestURI().getPath())) {
                response = gson.toJson(CACHE.getAll());
            } else {
                response = gson.toJson(Map.of("paths", new String[] { "/all", "/latest" }));
            }

            setResponseHeaders(exchange, 200, response.length());
            return response;
        }

        private String doPOST(HttpExchange exchange) throws IOException {
            String response = "";
            String path = exchange.getRequestURI().getPath();
            Map<String, List<String>> headers = exchange.getRequestHeaders();

            String ctype = exchange.getRequestHeaders().getFirst("Content-Type");
            // refuse to receive non-json content
            if (!"application/json".equals(ctype)) {
                try {
                    setResponseHeaders(exchange, 400, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            InputStream is = exchange.getRequestBody();
            int contentLength = Integer.parseInt(exchange.getRequestHeaders().getFirst("Content-Length"));
            byte[] data = new byte[contentLength];
            int bytesRead = 0;
            int offset = 0;
            while (offset < contentLength && (bytesRead = is.read(data, offset, contentLength - offset)) != -1) {
                offset += bytesRead;
            }
            String postData = new String(data, StandardCharsets.UTF_8);
            String signatures = exchange.getRequestHeaders().getFirst("X-PORTCAST-SIGNATURE");
            String pushTimestamp = exchange.getRequestHeaders().getFirst("X-PORTCAST-PUSH-TIMESTAMP");
            boolean verified = VerifySignatures.verifySignatures(pushTimestamp, postData, signatures);
            ZonedDateTime datetimePushTimestamp = DateUtil.parseTimestamp(pushTimestamp);
            ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
            Map<String, String> reasons = new HashMap<>();
            boolean validTimestamp = datetimePushTimestamp != null
                    && ChronoUnit.MINUTES.between(datetimePushTimestamp, now) <= 5;
            if (verified && validTimestamp) {
                System.out.println("use data");
            } else {
                System.out.println("discard data");
                logging.info(
                        String.format("POST request,%nPath: %s%nHeaders:%n%s%n%nBody:%n%s%n", path, headers, postData));
            }

            reasons.put("signature", verified ? "valid" : "invalid");
            reasons.put("timestamp", validTimestamp ? "Available" : "No valid timestamp provided.");
            String replayed = datetimePushTimestamp == null ? "No timestamp provided."
                    : (validTimestamp ? "Timestamp within 5 minutes." : "Timestamp is too old. Possible replay.");
            reasons.put("replayed", replayed);
            Map<String, Object> store = new HashMap<>();
            store.put("data", gson.fromJson(postData, Map.class)); // Assuming JSON to Map parsing
            store.put("received_at", now.toString());
            store.put("headers",
                    Map.of("X-PORTCAST-SIGNATURE", signatures, "X-PORTCAST-PUSH-TIMESTAMP", pushTimestamp));
            store.putAll(reasons);
            CACHE.add(store);
            response = gson.toJson(store);
            setResponseHeaders(exchange, 200, response.length());

            return response;
        }
    }
}
