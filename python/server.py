import base64
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json
from datetime import datetime, timedelta, timezone

from Crypto.Hash import SHA512
from Crypto.PublicKey import ECC
from Crypto.Signature import eddsa


HOST_NAME = "0.0.0.0"
SERVER_PORT = 10100


class PushCache:
    def __init__(self) -> None:
        self.latest_push = []

    def add(self, data):
        self.latest_push.append(data)
        # keep only the latest
        self.latest_push = self.latest_push[-30:]

    def get_latest(self):
        return self.latest_push[-1]

    def get_all(self):
        return self.latest_push


CACHE = PushCache()


def verify_v2_signature(timestamp: str, data: str, signature: str) -> bool:
    """This is cross-language signature and should work fine across different
    programming languages.
    """
    message = (timestamp + data).replace(" ", "")
    key = ECC.import_key(open("public_ed25519.pub").read())
    verifier = eddsa.new(key, "rfc8032")
    try:
        signature_decoded = base64.standard_b64decode(signature)
        verifier.verify(message.encode("utf-8"), signature_decoded)
        print("The message is authentic", flush=True)
        return True
    except ValueError:
        print("The message is not authentic", flush=True)
    return False


def verify_v1_signature(timestamp: str, data: str, signature: str) -> bool:
    message = (timestamp + data).replace(" ", "")
    h = SHA512.new(message.encode("utf-8"))
    key = ECC.import_key(open("public_ed25519.pub").read())
    verifier = eddsa.new(key, "rfc8032")
    try:
        signature_decoded = base64.standard_b64decode(signature)
        verifier.verify(h, signature_decoded)
        print("The message is authentic", flush=True)
        return True
    except ValueError:
        print("The message is not authentic", flush=True)
    return False


def verify_signatures(timestamp: str, data: str, signatures: str) -> bool:
    """
    For a signature extract the version and actual signature
    example: v1=uFiKi3t+sn19JHF1HjFRz937r0ZXRY2UiY3BdW0cmlF2R1JtpxyvsY9MBZoAj74Km+G1vWtDKOxhx437LBYaBw==
    version: v1
    signature: uFiKi3t+sn19JHF1HjFRz937r0ZXRY2UiY3BdW0cmlF2R1JtpxyvsY9MBZoAj74Km+G1vWtDKOxhx437LBYaBw==

    Signature are base64 encoded so no , will be present except to include multiple signatures.
    Multiple signature can be included.
    v1=uFiKi3t+sn19JHF1HjFRz937r0ZXRY2UiY3BdW0cmlF2R1JtpxyvsY9MBZoAj74Km+G1vWtDKOxhx437LBYaBw==,v1=SZz3uBgI5VHQypFA29vl2bbnqNiS3NMxwqbn4Ls+MPukeovDppMAU1Ce1054mErCV3sD7vm1LgtQC1+eVdXrBw==,v2=P8SotgRy+FPh6DCIDZ0uZbVMG+sy2a5UppN10k1FG17rDLMGzdL9h9L8FwWAEj/rJ/y3jVZYUgsL+mT9wRBmBg==
    Generally used to rotate keys or change the actual signature

    Since multiple signatures are present, only one needs to be valid.
    This is because key rotation, version upgrade, etc, can be in effect, and
    this application might not yet have the latest keys.

    Args:
        timestamp (str): timestamp present in the headers
        data (str): data provided
        signature (str): the signature
    """
    signature_list = signatures.split(",")
    if not signature_list:
        return False

    verified = False
    for versioned_signature in signature_list:
        if not versioned_signature:
            continue
        sig_split = versioned_signature.split("=", 1)
        if len(sig_split) != 2:
            continue
        version = sig_split[0]
        signature = sig_split[1]
        if version == "v1":
            verified = verified or verify_v1_signature(timestamp, data, signature)
        elif version == "v2":
            verified = verified or verify_v2_signature(timestamp, data, signature)

    return verified


def parse_timestamp(timestamp):
    try:
        parse_datetime = datetime.fromisoformat(timestamp)
        # timestamps should have timezone
        if parse_datetime.tzinfo is not None:
            return parse_datetime
    except Exception:
        pass
    return None


# modified from https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7
class Server(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def do_GET(self):
        print(self.path)
        logging.info(
            "GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers)
        )
        self._set_response()
        if self.path == "/latest":
            self.wfile.write(json.dumps(CACHE.get_latest()).encode("utf-8"))
        elif self.path == "/all":
            # for i in CACHE.get_all():
            self.wfile.write(json.dumps(CACHE.get_all()).encode("utf-8"))
        else:
            self.wfile.write(json.dumps({"paths": ["/all", "/latest"]}).encode("utf-8"))

    def do_POST(self):
        ctype = self.headers.get("content-type")
        # refuse to receive non-json content
        if ctype != "application/json":
            self.send_response(400)
            self.end_headers()
            return

        content_length = int(
            self.headers["Content-Length"]
        )  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        signatures = self.headers.get("X-PORTCAST-SIGNATURE", "")
        push_timestamp = self.headers.get("X-PORTCAST-PUSH-TIMESTAMP", "")
        verified = verify_signatures(
            push_timestamp, post_data.decode("utf-8"), signatures
        )
        datetime_push_timestamp = parse_timestamp(push_timestamp)
        now = datetime.now(tz=timezone.utc)
        reasons = {}
        valid_timestamp = datetime_push_timestamp and datetime_push_timestamp > (
            now - timedelta(minutes=5)
        )
        reasons = {}
        if verified and valid_timestamp:
            print("use data", flush=True)
        else:
            print("discard data", flush=True)
            logging.info(
                "POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path),
                str(self.headers),
                post_data.decode("utf-8"),
            )

        reasons["signature"] = "valid" if verified else "invalid"
        reasons["timestamp"] = (
            "Available" if valid_timestamp else "No valid timestamp provided."
        )
        if datetime_push_timestamp is not None:
            replayed = (
                "Timestamp within 5 minutes."
                if valid_timestamp
                else "Timestamp is too old. Possible replay."
            )
        else:
            replayed = None
        reasons["replayed"] = replayed
        store = {
            "data": json.loads(post_data),
            "received_at": now.isoformat(),
            "headers": {
                "X-PORTCAST-SIGNATURE": signatures,
                "X-PORTCAST-PUSH-TIMESTAMP": push_timestamp,
            },
            **reasons,
        }
        CACHE.add(store)
        self._set_response()
        self.wfile.write(json.dumps(store).encode("utf-8"))


if __name__ == "__main__":
    webServer = HTTPServer((HOST_NAME, SERVER_PORT), Server)
    print("Server started http://%s:%s" % (HOST_NAME, SERVER_PORT))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
