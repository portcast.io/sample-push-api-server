package io.portcast.app.java_springboot;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import java.util.logging.Logger;
import com.google.gson.Gson;

import jakarta.servlet.http.HttpServletRequest;

@SpringBootApplication
@RestController
public class Server {

	private static final Logger logging = Logger.getLogger(Server.class.getName());
	private static final String HOST_NAME = "0.0.0.0";
	private static final Integer SERVER_PORT = 10102;
	private static PushCache CACHE = new PushCache();
	private static final Gson gson = new Gson();

	@GetMapping("/latest")
	public String doGetLatest() {
		return gson.toJson(CACHE.getLatest());
	}

	@GetMapping("/all")
	public String doGetAll() {
		return gson.toJson(CACHE.getAll());
	}

	@GetMapping("/**")
	public String doGetRoot() {
		return gson.toJson(Collections.singletonMap("paths", List.of("/all", "/latest")));
	}

	@PostMapping("/**")
	public ResponseEntity<String> doPost(@RequestHeader Map<String, String> headers,
			@RequestBody(required = false) String body,
			HttpServletRequest request) {
		String response = "";
		String path = request.getRequestURI();

		String ctype = headers.get("content-type");
		// refuse to receive non-json content
		if (!"application/json".equals(ctype)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Content-Type must be application/json");
		}

		String contentLengthStr = headers.get("content-length");
		int contentLength = Integer.parseInt(contentLengthStr);
		String postData = body.substring(0, Math.min(body.length(), contentLength));
		// generally headers are case insensitive but case is generally conserved.
		// However, SpringBoot converts all header keys into small case. So, just
		// handling both.
		String signatures = headers.getOrDefault("X-PORTCAST-SIGNATURE", headers.get("x-portcast-signature"));
		String pushTimestamp = headers.getOrDefault("X-PORTCAST-PUSH-TIMESTAMP",
				headers.get("x-portcast-push-timestamp"));
		boolean verified = VerifySignatures.verifySignatures(pushTimestamp, postData, signatures);
		ZonedDateTime datetimePushTimestamp = DateUtil.parseTimestamp(pushTimestamp);
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
		Map<String, String> reasons = new HashMap<>();
		boolean validTimestamp = datetimePushTimestamp != null
				&& ChronoUnit.MINUTES.between(datetimePushTimestamp, now) <= 5;
		if (verified && validTimestamp) {
			System.out.println("use data");
		} else {
			System.out.println("discard data");
			logging.info(
					String.format("POST request,%nPath: %s%nHeaders:%n%s%n%nBody:%n%s%n", path, headers, postData));
		}

		reasons.put("signature", verified ? "valid" : "invalid");
		reasons.put("timestamp", validTimestamp ? "Available" : "No valid timestamp provided.");
		String replayed = datetimePushTimestamp == null ? "No timestamp provided."
				: (validTimestamp ? "Timestamp within 5 minutes." : "Timestamp is too old. Possible replay.");
		reasons.put("replayed", replayed);
		Map<String, Object> store = new HashMap<>();
		store.put("data", gson.fromJson(postData, Map.class)); // Assuming JSON to Map parsing
		store.put("received_at", now.toString());
		store.put("headers",
				Map.of("X-PORTCAST-SIGNATURE", signatures, "X-PORTCAST-PUSH-TIMESTAMP", pushTimestamp));
		store.putAll(reasons);
		CACHE.add(store);
		response = gson.toJson(store);

		return ResponseEntity.ok(response);
	}

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(Server.class);
		application.setDefaultProperties(Collections.singletonMap("server.port", SERVER_PORT));
		application.run(args);
		System.out.println("Server started at http://" + HOST_NAME + ":" + SERVER_PORT);
	}

}
