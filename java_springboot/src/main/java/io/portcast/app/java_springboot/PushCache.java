//* Same implementation as in native java sample push server: java_/src/main/java/io/portcast/app/PushCache.java */
package io.portcast.app.java_springboot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PushCache {
    private List<Map<String, Object>> latestPush;

    public PushCache() {
        this.latestPush = new ArrayList<>();
    }

    public void add(Map<String, Object> data) {
        latestPush.add(data);
        // Keep only the latest 30 elements
        if (latestPush.size() > 30) {
            latestPush = latestPush.subList(latestPush.size() - 30, latestPush.size());
        }
    }

    public Map<String, Object> getLatest() {
        return latestPush.get(latestPush.size() - 1);
    }

    public List<Map<String, Object>> getAll() {
        return new ArrayList<>(latestPush);
    }
}
