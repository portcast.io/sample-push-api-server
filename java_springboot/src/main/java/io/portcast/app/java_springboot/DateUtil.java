//* Same implementation as in native java sample push server: java_/src/main/java/io/portcast/app/DateUtil */
package io.portcast.app.java_springboot;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

public class DateUtil {
    public static ZonedDateTime parseTimestamp(String timestamp) {
        try {
            OffsetDateTime parseDatetime = OffsetDateTime.parse(timestamp);
            return parseDatetime.toZonedDateTime();
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
