//* Same implementation as in native java sample push server: java_/src/main/java/io/portcast/app/VerifySignatures */
package io.portcast.app.java_springboot;

import java.nio.charset.StandardCharsets;
import java.io.File;
import com.sshtools.common.publickey.SshKeyUtils;
import com.sshtools.common.ssh.components.SshPublicKey;
import com.sshtools.common.util.Base64;

public class VerifySignatures {
    public static boolean verifyV2Signature(String timestamp, String data, String signature) {
        try {
            String message = timestamp + data.replace(" ", "");
            byte[] messageBytes = message.getBytes(StandardCharsets.UTF_8);

            // read public key
            SshPublicKey readPublicKey = SshKeyUtils.getPublicKey(new File("public_ed25519.pub"));

            // verify signature
            byte[] signatureDecoded = Base64.decode(signature);
            boolean isVerified = readPublicKey.verifySignature(signatureDecoded, messageBytes);

            if (isVerified) {
                System.out.println("The message is authentic");
            } else {
                System.out.println("The message is not authentic");
            }
            return isVerified;
        } catch (Exception e) {
            System.out.println("The message is not authentic");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean verifySignatures(String timestamp, String data, String signatures) {
        String[] signatureList = signatures.split(",");
        if (signatureList.length == 0) {
            return false;
        }

        boolean verified = false;
        for (String versionedSignature : signatureList) {
            if (versionedSignature == null || versionedSignature.isEmpty()) {
                continue;
            }
            String[] sigSplit = versionedSignature.split("=", 2);
            if (sigSplit.length != 2) {
                continue;
            }
            String version = sigSplit[0];
            String signature = sigSplit[1];

            // NOTE: Earlier than v2 signature is not supported
            if ("v2".equals(version)) {
                verified = verified || verifyV2Signature(timestamp, data, signature);
            }
        }

        return verified;
    }

}
