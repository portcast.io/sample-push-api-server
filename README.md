# Sample Push API Server

- [Sample Push API Server](#sample-push-api-server)
  - [Summary](#summary)
  - [Run the server](#run-the-server)
    - [Python](#python)
    - [Java](#java)
      - [Native Java](#native-java)
      - [SpringBoot based Java](#springboot-based-java)
    - [NodeJS](#nodejs)
  - [License](#license)

## Summary

This is a sample server implementation to showcase how to integrate with Portcast Push
API framework for clients and SHOULD *NOT BE USED IN PRODUCTION*

It is a reference implementation only.

The server will need a public ip address for Portcast to be able to reach it. Once it is
set up, use the callback url <https://ip:port>.

Documentation on Portcast's API can be found here <http://portcast.stoplight.io>

## Run the server

### Python

Using Docker

```bash
docker build -t example-push-api-server-python -f Dockerfile.python .
docker run -ti -p 10100:10100/tcp example-push-api-server-python
```

Without Docker

```bash
# Create and activate a virtual environment. You can use pyenv.
pip install -r python/requirements.txt

python python/server.py
```

### Java

We have two implementations for Java. One is native using low-level `httpserver` to
showcase the underlying implementation to verify signature. And, `SpringBoot` based
implementation to show a more realistic example as generally clients would use that.

#### Native Java

Native Java implementation is in folder `java_` and uses `maven` for dependency
management.

**Note:** Files `java_/src/main/java/io/portcast/app/DateUtil.java`,
`java_/src/main/java/io/portcast/app/PushCache.java` and
`java_/src/main/java/io/portcast/app/VerifySignatures.java` are common with respective
files in SprintBoot based implementation. Only change is in respective `Server.java`
files.

```bash
cd java_

mvn exec:java -Dexec.mainClass="io.portcast.app.Server"
```

#### SpringBoot based Java

SpringBoot Java implementation is in folder `java_springboot` and uses `maven` for
dependency management.

```bash
cd java_springboot

./mvnw spring-boot:run
```

### NodeJS

Nodejs implementation is in folder nodejs. It uses expressjs as server backend.
Run locally:
```bash
cd nodejs

npm install

node server.js
```
Docker
```
docker build -f Dockerfile.nodejs -t sample-nodejs-push-server .

docker run -p 10103:10103 sample-nodejs-push-server
```


## License

Feel free to copy... though we strongly discourage that.
