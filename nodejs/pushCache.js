class PushCache {
    constructor() {
        this.latestPush = [];
    }

    add(data) {
        this.latestPush.push(data);
        // Keep only the latest 30 items
        this.latestPush = this.latestPush.slice(-30);
    }

    getLatest() {
        return this.latestPush[this.latestPush.length - 1];
    }

    getAll() {
        return this.latestPush;
    }
}

module.exports = PushCache;