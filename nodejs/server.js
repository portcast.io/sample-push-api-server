const sshpk = require('sshpk');
var fs = require('fs');
const path = require('path');
const Cache = require('./pushCache');
const express = require('express');
const rawBody = require('raw-body');
const moment = require('moment-timezone');

const app = express()
const cache = new Cache()
const filePath = path.join(__dirname, 'public_ed25519.pub');
const keyPub = fs.readFileSync(filePath);
const hostname = '127.0.0.1';
const port = 10103;

// Middleware to check required headers for POST requests
function validateHeaders(req, res, next) {
    const signature = req.header('X-PORTCAST-SIGNATURE');
    const timestamp = req.header('X-PORTCAST-PUSH-TIMESTAMP');

    if (!signature || !timestamp) {
        return res.status(400).send('Missing required headers');
    }

    next();
}

function verifyV2Signature(timestamp, data, signature) {
	const message = (timestamp + data).replaceAll(' ', '');

	/* Read in an OpenSSH-format public key */
	var key = sshpk.parseKey(keyPub, 'ssh');

	var verify = key.createVerify('sha512');
	verify.update(message);

	const valid = verify.verify(signature);
	return valid
}


function parseTimestamp(timestamp) {
    try {
        // Parse the timestamp using moment
        const parseDatetime = moment.utc(timestamp, moment.ISO_8601);

        const timezoneRegex = /([+-]\d{2}:\d{2})$/;
        
        // Check if the timestamp is valid and has a timezone
        if (parseDatetime.isValid() &&  timezoneRegex.test(timestamp)) {
            return parseDatetime;
        }
    } catch (error) {
        return null
    }
    return null;
}

function validateTimestamp(pushTimestamp) {
    const datetimePushTimestamp = parseTimestamp(pushTimestamp);
    const now = moment().tz('UTC');
    
    const validTimestamp = datetimePushTimestamp && datetimePushTimestamp.isAfter(now.subtract(5, 'minutes'));

    return validTimestamp

}

function verifySignatures(signatures, timestamp, data) {
    const signatureList = signatures.split(",");
    if (!signatureList.length) {
        return false;
    }

    let verified = false;
    for (const versionedSignature of signatureList) {
        if (!versionedSignature) {
            continue;
        }
        // const sigSplit = versionedSignature.split("=");
        // if (sigSplit.length !== 2) {
        //     continue;
        // }
		let [version, ...rest] = versionedSignature.split("=");
		const signature = rest.join("=");

        // const version = sigSplit[0];
        // const signature = sigSplit[1];
        if (version === "v1") {
            continue;
        } else if (version === "v2") {
            verified = verifyV2Signature(timestamp, data, signature)
        }
    }

    return verified;
}

app.post('/', validateHeaders, (req, res) => {
    rawBody(req, {
        length: req.headers['content-length'],
        limit: '1mb',
        encoding: 'utf8' // You can change this to 'binary' or null to get the raw Buffer
    }, (err, string) => {
        if (err) return res.status(500).send(err.toString());

		const data = string;
		if (!data) {
			return res.status(400).send('Missing data');
		}

		var timestamp = req.header('X-PORTCAST-PUSH-TIMESTAMP');
		var signature = req.header('X-PORTCAST-SIGNATURE');

		var verified  = verifySignatures(signature, timestamp, data); 
		var validTimestamp = validateTimestamp(timestamp)
		var reasons = {};

		reasons["signature"] = verified ? "valid" : "invalid";
		reasons["timestamp"] = validTimestamp? "Available" : "No valid timestamp provided.";

		reasons["replayed"] = validTimestamp ? "Timestamp within 5 minutes." : "Timestamp is too old. Possible replay.";

		const store = {
			data: data,
			received_at: moment.utc().toISOString(),
			headers: {
				"X-PORTCAST-SIGNATURE": signature,
				"X-PORTCAST-PUSH-TIMESTAMP": timestamp,
			},
			...reasons,
		};
		

		cache.add(store);
		res.send(store)
    });
});


// GET request handler for "/latest"
app.get('/latest', (req, res) => {
    const latest = cache.getLatest();
    if (!latest) {
        return res.status(404).send('No data available');
    }
    res.status(200).json(latest);
});

// GET request handler for "/all"
app.get('/all', (req, res) => {
    const allData = cache.getAll();
    res.status(200).json(allData);
});

// Start the server
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});